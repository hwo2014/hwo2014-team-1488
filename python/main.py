import json
import socket
import sys


class NoobBot(object):

    def __init__(self, socket, name, key):
        self.socket = socket
        self.name = name
        self.key = key
        self.trackinfo=[]
        self.debug=0
        self.piece=-1

    def msg(self, msg_type, data):
#         print "sending"
        self.send(json.dumps({"msgType": msg_type, "data": data}))

    def send(self, msg):
        self.socket.sendall(msg + "\n")

    def join(self):
        print "sending join"
        return self.msg("join", {"name": self.name,
                                 "key": self.key})
    def join_race(self):
        return  self.msg("joinRace",{"botId":{"name": self.name,"key": self.key},"carCount": 3})
 

    def throttle(self, throttle):
        self.msg("throttle", throttle)

    def ping(self):
        self.msg("ping", {})

    def run(self):
        self.join()
        #self.join_race()
        self.msg_loop()

    def on_join(self, data):
        print("Joined")
        self.ping()
        
    def on_join_race(self, data):
        print("Joined race")
        self.ping()

    def on_game_start(self, data):
        print("Race started")
        self.ping()

    def on_car_positions(self, data):
        
        for pos in data:
            pieceinx= pos["piecePosition"]["pieceIndex"]
            self.piece=pieceinx
           # print pieceinx, len(self.trackinfo)
            if pieceinx+1 < len(self.trackinfo):
                if (self.trackinfo[pieceinx]["type"]==0 or self.trackinfo[pieceinx]["type"]==1)  and self.trackinfo[pieceinx+1]["type"]==2 :
                    if (self.trackinfo[pieceinx]["length"]-pos["piecePosition"]["inPieceDistance"])<(0.4*self.trackinfo[pieceinx]["length"]):
                        self.throttle(0.4)
                      #  print "sending throttle 1"
                    else:
                        self.throttle(self.trackinfo[pieceinx]["throttle"])
                else:
                    self.throttle(self.trackinfo[pieceinx]["throttle"])
                    #print "sending throttle 2"
            else:
                self.throttle(self.trackinfo[pieceinx]["throttle"])
                #print "sending throttle 3"
            
            
#         if self.debug==0:
#             self.debug=1
        #self.throttle(0.6)

    def on_crash(self, data):
        print("Someone crashed at piece ",self.piece)
        self.ping()

    def on_game_end(self, data):
        print("Race ended")
        self.ping()

    def on_error(self, data):
        print("Error: {0}".format(data))
        self.ping()
    
    def on_your_car(self,data):
        print "your car recieved"
        self.ping()
           
    def on_game_init(self,data):
        print "game init recieved"
        print data
        for node in data["race"]["track"]["pieces"]:
            if 'length' in node.keys():
                node["type"]=0
                node["throttle"]=0.9
            if 'switch' in node.keys():
                node["type"]=1
                node["throttle"]=0.7
            if 'radius' in node.keys():
                node["type"]=2
                node["length"]=(2*3.1416*node["radius"]*node["angle"])/360
                node["throttle"]=0.5
                 
            self.trackinfo.append(node)
        #print self.trackinfo
                
                
        
        self.ping()

    def msg_loop(self):
        msg_map = {
            'join': self.on_join,
            'joinRace':self.on_join_race,
            'gameStart': self.on_game_start,
            'yourCar':self.on_your_car,
            'gameInit':self.on_game_init,
            'carPositions': self.on_car_positions,
            'crash': self.on_crash,
            'gameEnd': self.on_game_end,
            'error': self.on_error,
        }
        socket_file = s.makefile()
        line = socket_file.readline()
        while line:
            msg = json.loads(line)
            msg_type, data = msg['msgType'], msg['data']
            if msg_type in msg_map:
                msg_map[msg_type](data)
            else:
                print("Got {0}".format(msg_type))
                self.ping()
            line = socket_file.readline()


if __name__ == "__main__":
    if len(sys.argv) != 5:
        print("Usage: ./run host port botname botkey")
    else:
        host, port, name, key = sys.argv[1:5]
        print("Connecting with parameters:")
        print("host={0}, port={1}, bot name={2}, key={3}".format(*sys.argv[1:5]))
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.connect((host, int(port)))
#         if s:
#             print "connected"
        bot = NoobBot(s, name, key)
        bot.run()
